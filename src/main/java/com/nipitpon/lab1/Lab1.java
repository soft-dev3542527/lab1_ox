/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.nipitpon.lab1;

import java.util.Scanner;

/**
 *
 * @author nipit
 */
public class Lab1 {

    public static Scanner sc = new Scanner(System.in);
    public static int playerX, playerO = 0;
    public static int pos;
    public static char[][] board = {{' ', ' ', ' '},
    {' ', ' ', ' '},
    {' ', ' ', ' '},};
    
    static void resetBoard(char[][] board) {
        board[0][0] = ' ' ;
        board[0][1] = ' ' ;
        board[0][2] = ' ' ;
        board[1][0] = ' ' ;
        board[1][1] = ' ' ;
        board[1][2] = ' ' ;
        board[2][0] = ' ' ;
        board[2][1] = ' ' ;
        board[2][2] = ' ' ;
        
    }

    static void showBoard() {
        System.out.println(board[0][0] + "|" + board[0][1] + "|" + board[0][2]);
        System.out.println("-+-+-");
        System.out.println(board[1][0] + "|" + board[1][1] + "|" + board[1][2]);
        System.out.println("-+-+-");
        System.out.println(board[2][0] + "|" + board[2][1] + "|" + board[2][2]);
        System.out.println("__________________________________________");
    }

    static void input() {
        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.print("Turn: " + checkPlayer());
            System.out.println();
            System.out.print("Please input 1-9: ");
            pos = sc.nextInt();

            changePosition();
            checkPlayer();

            if (chechWin(board, 'X') == false) {
                break;
            };
            if (chechWin(board, 'O') == false) {
                break;
            };
            changePlayer();

        }

    }

    private static boolean chechWin(char[][] board, char symbol) {
        if ((board[0][0] == symbol && board[0][1] == symbol && board[0][2] == symbol)
                || (board[1][0] == symbol && board[1][1] == symbol && board[1][2] == symbol)
                || (board[2][0] == symbol && board[2][1] == symbol && board[2][2] == symbol)
                || (board[0][0] == symbol && board[1][0] == symbol && board[2][0] == symbol)
                || (board[0][1] == symbol && board[1][1] == symbol && board[2][1] == symbol)
                || (board[0][2] == symbol && board[1][2] == symbol && board[2][2] == symbol)
                || (board[0][0] == symbol && board[1][1] == symbol && board[2][2] == symbol)
                || (board[0][2] == symbol && board[1][1] == symbol && board[2][0] == symbol)) {
            System.out.println(symbol+": Win!!");
            return false;
        }
        return true;
    }

    static void changePosition() {
//        System.out.println(pos);
        switch (pos) {
            case 1:
                board[0][0] = checkPlayer();
                break;
            case 2:
                board[0][1] = checkPlayer();
                break;
            case 3:
                board[0][2] = checkPlayer();
                break;
            case 4:
                board[1][0] = checkPlayer();
                break;
            case 5:
                board[1][1] = checkPlayer();
                break;
            case 6:
                board[1][2] = checkPlayer();
                break;
            case 7:
                board[2][0] = checkPlayer();
                break;
            case 8:
                board[2][1] = checkPlayer();
                break;
            case 9:
                board[2][2] = checkPlayer();
                break;
            default:
                throw new AssertionError();
        }
        showBoard();
    }

    public static void main(String[] args) {

        while (true) {
            
            if(startGame()==false){
                break;
            };
        }
    }

    public static char checkPlayer() {
        if (playerX == 1) {
            return 'X';
        }
        if (playerO == 1) {
            return 'O';
        }

        return '0';
    }

    public static void changePlayer() {
        if (playerX == 1) {
            playerO = 1;
            playerX = 0;
        } else if (playerO == 1) {
            playerX = 1;
            playerO = 0;
        }
//        System.out.println("CheckPlayer#2 " + playerX + " " + playerO);
    }

    public static boolean startGame() {
        System.out.println("Welcome to OX game!!");
        System.out.println("Please Select First Player");
        System.out.print("Choose X Input 1 | O Input 2: ");
        int num = sc.nextInt();
        if (num == 1) {
            playerX = 1;
        } else {
            playerO = 1;
        }
        System.out.println(checkPlayer());
//        System.out.println("CheckPlayer#1 " + playerX + " " + playerO);
        System.out.println("_________________________________");
        System.out.println("");
        System.out.println("Start!!");
        showBoard();
        input();
        resetBoard(board);
        
        System.out.print("Do you want to play again? (y/n)");
        String ans = sc.next();
        if (ans == "y") {
            return true;
        } else if(ans == "n"){
        return false;
        }
        return false;
        
    }
}
